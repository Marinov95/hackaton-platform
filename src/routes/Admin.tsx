import { Outlet, useNavigate } from 'react-router-dom'
import Sidebar from '../components/Sidebar'
import { useLocation } from 'react-router-dom'

import { useEffect } from 'react'

export default function Admin() {
    const navigate = useNavigate()
    const location = useLocation()
    console.log(location)
    useEffect(() => {
        const isLoggedIn = localStorage.getItem('token')
        console.log(location.pathname)
        if (isLoggedIn === null) {
            navigate('/login')
        } else {
            navigate(
                location.pathname.length > 1 ? location.pathname : '/admin/home'
            )
        }
    }, [location.pathname])

    return (
        <>
            <div className="flex">
                <Sidebar />
                <div className="my-10 flex-1">
                    <Outlet />
                </div>
            </div>
        </>
    )
}
