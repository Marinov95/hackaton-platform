import InputGroup from '../components/InputGroup'
import { Form, Field } from 'react-final-form'
import loginBanner from '../assets/images/login-banner.png'
import logo from '../assets/images/brainster-logo.png'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { Transition } from '@headlessui/react'

const CONTENT_CARD_CONTENT = [
    {
        title: 'Engage and cultivate your community',
        content:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    },
    {
        title: 'Create a seamless experience for atendees',
        content:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    },
    {
        title: 'Reach wider audience and grow your attendance',
        content:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    },
]

export default function Login() {
    const navigate = useNavigate()

    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    async function onSubmit(form: typeof Form) {
        console.log(form)
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        headers.append('Access-Control-Allow-Origin', '*')
        headers.append('Access-Control-Allow-Credentials', 'true')

        try {
            setError('')
            setLoading(true)
            const response = await fetch(
                'https://hristina-m-kjiroski.sharedwithexpose.com/api/login',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(form),
                }
            )
            const data = await response.json()
            if (data.token) {
                localStorage.setItem('token', data.token.toString())

                navigate('/admin/home')
            } else {
                setError('Invalid credentials')
            }
            setLoading(false)
        } catch (err) {
            setLoading(false)
            console.log(err)
        }
    }

    function validate(form: typeof Form) {
        const errors = {} as any

        if (!form.email) {
            errors.email = 'Required'
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(form.email)
        ) {
            errors.email = 'Invalid email address'
        }

        if (!form.password) {
            errors.password = 'Required'
        } else if (form.password.length < 6) {
            errors.password = 'Password must be at least 6 characters'
        }
        console.log(errors)

        return errors
    }
    return (
        <>
            <div className="overflow-x-hidden">
                <h1 className="text-[25px] font-medium mt-2 ml-9 text-[#45C9C8]">
                    EVENTOR
                </h1>
                <div className="grid grid-cols-12  gap-14 p-10">
                    <div className="col-span-4 leading-tight">
                        <h1 className="text-[80px] mb-8 text-left ">
                            Welcome to EVENTOR!
                        </h1>
                        <div className='leading-normal py-14 rounded-2xl border-2 border-gray-400 min-h-[464px]"'>
                            <div className="w-1/2 mx-auto">
                                <Form
                                    onSubmit={onSubmit}
                                    validate={validate}
                                    render={({ handleSubmit }) => (
                                        <form
                                            onSubmit={handleSubmit}
                                            className="space-y-4"
                                        >
                                            <Field
                                                name="email"
                                                type="email"
                                                render={({ input, meta }) => (
                                                    <InputGroup
                                                        label="E-mail"
                                                        input={input}
                                                        meta={meta}
                                                        placeholder="example@example.com"
                                                        name="email"
                                                    />
                                                )}
                                            />

                                            <Field
                                                name="password"
                                                type="password"
                                            >
                                                {({ input, meta }) => (
                                                    <InputGroup
                                                        meta={meta}
                                                        input={input}
                                                        label="Password"
                                                        placeholder="******"
                                                        name="password"
                                                    />
                                                )}
                                            </Field>
                                            <Transition
                                                as="p"
                                                show={
                                                    error.length ? true : false
                                                }
                                                enter="transition ease-out duration-100"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                                className="text-red-500 text-sm"
                                            >
                                                {error}
                                            </Transition>
                                            <button
                                                type="submit"
                                                className="text-center bg-[#45C9C8] text-white font-semibold rounded-lg py-2 w-full text-[18px] disabled:bg-gray-400 disabled:opacity-80"
                                                disabled={loading}
                                            >
                                                {loading ? (
                                                    <i className="fa-solid fa-spinner  animate-spin duration-300"></i>
                                                ) : (
                                                    'Sign in'
                                                )}
                                            </button>
                                            <div className="text-center my-2">
                                                <a>Forgot password?</a>
                                            </div>
                                        </form>
                                    )}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-start-6 col-span-7">
                        <img
                            src={loginBanner}
                            className="w-full mb-6 h-[448px]"
                            alt=""
                        />

                        <h2 className="text-6xl mb-4 w-3/4 mx-auto font-bold text-center">
                            Always more to discover and learn
                        </h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and
                            scrambled it to make a type specimen book. Lorem
                            Ipsum is simply dummy text of the printing.
                        </p>
                    </div>

                    {CONTENT_CARD_CONTENT.map(({ title, content }, i) => (
                        <ContentCard title={title} key={i} content={content} />
                    ))}
                </div>
                <footer className="w-screen h-[64px] bg-[#9043B6] px-[100px] flex justify-between">
                    <div className="flex items-center h-full">
                        <img src={logo} alt="" />
                        <div className="text-[white] border-2 border-white rounded-full ml-3 w-[140px] text-center">
                            <h1>SCiDev LOGO</h1>
                        </div>
                    </div>
                    <div className="text-[white] flex items-center h-full">
                        <div className="text-[13px] font-light mr-9">
                            Privacy Policy
                        </div>
                        <div className="flex items-center">
                            <i className="fa-regular fa-copyright mr-2"></i>
                            <div className="text-[13px] font-light">
                                Copyright Reserved
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </>
    )
}

function ContentCard({
    title,
    content,
}: {
    title: string
    content: string
}): JSX.Element {
    return (
        <div className="col-span-4 p-6 rounded-lg bg-[#E3E0EF]">
            <h2 className="text-5xl w-[75%] font-bold mb-4">{title}</h2>
            <p className="text-lg">{content}</p>
        </div>
    )
}
