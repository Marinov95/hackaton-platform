import { Form, Field } from 'react-final-form'
import InputGroup from '../components/InputGroup'
import { Transition } from '@headlessui/react'
import { useLocation } from 'react-router-dom'
import { useState } from 'react'
import applicationFormImage from '../assets/images/application-form-img.png'
import fetchWithAuth from '../components/FetchWithAuth'
import { useEffect } from 'react'
import clsx from 'clsx'

export default function Hackaton() {
    const location = useLocation()
    const splitUrl = location.pathname.split('/')
    const hackatonId = splitUrl.pop()
    console.log(hackatonId)
    const [event, setEvent] = useState({} as any)

    const [loading, setLoading] = useState(false)

    const eventIsEmpty = Object.keys(event).length === 0

    useEffect(() => {
        async function getHackaton() {
            try {
                const res = await fetchWithAuth(
                    'https://hristina-m-kjiroski.sharedwithexpose.com/api/events'
                )
                const data = await res.json()
                setEvent(
                    data.event.find(
                        (event: any) =>
                            event.id.toString() === hackatonId?.toString()
                    )
                )
            } catch (err) {
                console.log(err)
            }
        }
        getHackaton()
        if (event.event && event.event[0]) {
            console.log(event.event[0].academies)
        }
    }, [])

    console.log(event)
    async function onSubmit(form: typeof Form) {
        console.log(form)
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        headers.append('Access-Control-Allow-Origin', '*')
        headers.append('Access-Control-Allow-Credentials', 'true')

        const serializedFormData = {
            full_name: form.fullName,
            email: form.email,
            phone: form.phoneNumber,
            available: !!form.availability,
            academy_id: parseInt(form.academy),
            participation_type_id: form.participationType,
        }

        try {
            setLoading(true)
            const response = await fetch(
                'https://hristina-m-kjiroski.sharedwithexpose.com/api/users',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(serializedFormData),
                }
            )
            await response.json()

            setLoading(false)
        } catch (err) {
            setLoading(false)
            console.log(err)
        }
    }

    function validate(form: typeof Form) {
        const errors = {} as any

        if (!form.fullName) {
            errors.fullName = 'Required'
        } else if (!/^[A-Za-z. '-]{1,}$/i.test(form.fullName)) {
            errors.fullName = 'Name cannot contain numbers'
        }

        if (!form.email) {
            errors.email = 'Required'
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(form.email)
        ) {
            errors.email = 'Invalid email address'
        }

        if (!form.phoneNumber) {
            errors.phoneNumber = 'Required'
        } else if (!/^\+\d{3}\d{2}\d{3}\d{3}$/i.test(form.phoneNumber)) {
            errors.phoneNumber = 'Invalid phone number'
        }
        if (!form.availability) {
            errors.availability = 'Required'
        }
        if (!form.academy) {
            errors.academy = 'Required'
        }
        console.log(errors)
        console.log(form)

        return errors
    }
    return (
        <>
            <div>
                <h1 className="text-[25px] font-medium mt-2 ml-9 text-[#45C9C8]">
                    EVENTOR
                </h1>
                <div className="px-[100px] leading-tight mt-16">
                    <h1 className="text-[80px] font-bold w-[60%]">
                        Welcome to the Eventor hackathon platform.
                    </h1>
                    <p className="w-[40%] text-[#919191] font-normal">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Possimus distinctio nemo omnis provident, aliquam
                        nostrum doloremque ratione amet labore fugiat odio, eius
                        dicta? Quod maxime architecto mollitia ex debitis ea,
                        dolores quis, earum, alias rem explicabo molestiae vel
                        corrupti.
                    </p>
                </div>
                <div className="grid gird-cols-12 gap-4 px-32 my-10">
                    <div className="col-span-5 overflow-y-auto h-[500px] ">
                        <div>
                            <Form
                                onSubmit={onSubmit}
                                validate={validate}
                                render={({ handleSubmit }) => (
                                    <form
                                        onSubmit={handleSubmit}
                                        className="space-y-4"
                                    >
                                        <Field
                                            name="fullName"
                                            type="text"
                                            render={({ input, meta }) => (
                                                <InputGroup
                                                    alternativestyle={true}
                                                    meta={meta}
                                                    input={input}
                                                    id="fullName"
                                                    label="Name and Surname"
                                                    placeholder="Your answer...."
                                                />
                                            )}
                                        />
                                        <Field
                                            name="email"
                                            type="email"
                                            render={({ input, meta }) => (
                                                <InputGroup
                                                    alternativestyle={true}
                                                    meta={meta}
                                                    input={input}
                                                    id="email"
                                                    label="E-mail"
                                                    placeholder="Your answer...."
                                                />
                                            )}
                                        />
                                        <Field
                                            name="phoneNumber"
                                            type="tel"
                                            render={({ input, meta }) => (
                                                <InputGroup
                                                    alternativestyle={true}
                                                    meta={meta}
                                                    input={input}
                                                    id="phoneNumber"
                                                    label="Phone Number"
                                                    placeholder="+389 70 000 000"
                                                />
                                            )}
                                        />

                                        <Field
                                            name="availability"
                                            type="radio"
                                            value="true"
                                            render={({ input, meta }) => (
                                                <>
                                                    <div className="h-[136px] flex flex-col justify-between py-3 px-6 rounded-md border-2 border-[#D9D9D9]">
                                                        <label>
                                                            Are you ready for
                                                            48h work and
                                                            available during the
                                                            hackathon?
                                                        </label>
                                                        <div className="flex gap-2 items-center">
                                                            {' '}
                                                            <input
                                                                name={
                                                                    input.name
                                                                }
                                                                type="radio"
                                                                value="true"
                                                                id="availability-true"
                                                                checked={
                                                                    input.checked
                                                                }
                                                                onChange={
                                                                    input.onChange
                                                                }
                                                            />
                                                            <label htmlFor="availability-true">
                                                                Yes
                                                            </label>
                                                        </div>
                                                        <Field
                                                            name="availability"
                                                            type="radio"
                                                            value="false"
                                                            render={({
                                                                input,
                                                            }) => (
                                                                <>
                                                                    {' '}
                                                                    <div className="flex gap-2 items-center">
                                                                        {' '}
                                                                        <input
                                                                            name={
                                                                                input.name
                                                                            }
                                                                            type="radio"
                                                                            value="false"
                                                                            id="availability-false"
                                                                            checked={
                                                                                input.checked
                                                                            }
                                                                            onChange={
                                                                                input.onChange
                                                                            }
                                                                        />
                                                                        <label htmlFor="availability-false">
                                                                            No
                                                                        </label>
                                                                    </div>
                                                                </>
                                                            )}
                                                        ></Field>
                                                    </div>
                                                    <Transition
                                                        as="p"
                                                        show={
                                                            meta.touched &&
                                                            meta.error
                                                                ? true
                                                                : false
                                                        }
                                                        enter="transition ease-out duration-100"
                                                        enterFrom="transform opacity-0 scale-95"
                                                        enterTo="transform opacity-100 scale-100"
                                                        leave="transition ease-in duration-75"
                                                        leaveFrom="transform opacity-100 scale-100"
                                                        leaveTo="transform opacity-0 scale-95"
                                                        className="text-red-500 text-sm ml-7"
                                                    >
                                                        {meta.error}
                                                    </Transition>
                                                </>
                                            )}
                                        />
                                        <div className="min-h-[136px] flex flex-col justify-between py-3 px-6 rounded-md border-2 border-[#D9D9D9] relative">
                                            <label>
                                                Which academy do you come from?
                                            </label>
                                            {eventIsEmpty ? (
                                                <i className="fa-solid fa-spinner  animate-spin duration-300 absolute left-6 top-14"></i>
                                            ) : (
                                                event.academies.map(
                                                    (item: any) => {
                                                        return (
                                                            <Field
                                                                name="academy"
                                                                type="radio"
                                                                key={item.id}
                                                                value={item.id}
                                                                render={({
                                                                    input,
                                                                    meta,
                                                                }) => (
                                                                    <>
                                                                        <div className="flex gap-2 items-center">
                                                                            {' '}
                                                                            <input
                                                                                name="academy"
                                                                                type="radio"
                                                                                value={
                                                                                    item.id
                                                                                }
                                                                                id={
                                                                                    item.id
                                                                                }
                                                                                checked={
                                                                                    input.checked
                                                                                }
                                                                                onChange={
                                                                                    input.onChange
                                                                                }
                                                                            />
                                                                            <label
                                                                                htmlFor={
                                                                                    item.id
                                                                                }
                                                                            >
                                                                                {
                                                                                    item.name
                                                                                }
                                                                            </label>
                                                                        </div>
                                                                        <Transition
                                                                            as="p"
                                                                            show={
                                                                                meta.touched &&
                                                                                meta.error
                                                                                    ? true
                                                                                    : false
                                                                            }
                                                                            enter="transition ease-out duration-100"
                                                                            enterFrom="transform opacity-0 scale-95"
                                                                            enterTo="transform opacity-100 scale-100"
                                                                            leave="transition ease-in duration-75"
                                                                            leaveFrom="transform opacity-100 scale-100"
                                                                            leaveTo="transform opacity-0 scale-95"
                                                                            className="text-red-500 text-sm ml-7"
                                                                        >
                                                                            {
                                                                                meta.error
                                                                            }
                                                                        </Transition>
                                                                    </>
                                                                )}
                                                            />
                                                        )
                                                    }
                                                )
                                            )}
                                        </div>
                                        <Field
                                            name="participationType"
                                            type="radio"
                                            value="1"
                                            render={({ input, meta }) => (
                                                <>
                                                    <div className="h-[136px] flex flex-col justify-between py-3 px-6 rounded-md border-2 border-[#D9D9D9]">
                                                        <label>
                                                            Will you be
                                                            participating online
                                                            or live?
                                                        </label>
                                                        <div className="flex gap-2 items-center">
                                                            {' '}
                                                            <input
                                                                name="participationType"
                                                                type="radio"
                                                                value="1"
                                                                id="participation-online"
                                                                checked={
                                                                    input.checked
                                                                }
                                                                onChange={
                                                                    input.onChange
                                                                }
                                                            />
                                                            <label htmlFor="participation-online">
                                                                Online
                                                            </label>
                                                        </div>
                                                        <Field
                                                            name="participationType"
                                                            type="radio"
                                                            value="2"
                                                            render={({
                                                                input,
                                                            }) => (
                                                                <>
                                                                    {' '}
                                                                    <div className="flex gap-2 items-center">
                                                                        {' '}
                                                                        <input
                                                                            name="participationType"
                                                                            type="radio"
                                                                            value="2"
                                                                            id="participation-live"
                                                                            checked={
                                                                                input.checked
                                                                            }
                                                                            onChange={
                                                                                input.onChange
                                                                            }
                                                                        />
                                                                        <label htmlFor="participation-live">
                                                                            Live
                                                                        </label>
                                                                    </div>
                                                                </>
                                                            )}
                                                        ></Field>
                                                    </div>
                                                    <Transition
                                                        as="p"
                                                        show={
                                                            meta.touched &&
                                                            meta.error
                                                                ? true
                                                                : false
                                                        }
                                                        enter="transition ease-out duration-100"
                                                        enterFrom="transform opacity-0 scale-95"
                                                        enterTo="transform opacity-100 scale-100"
                                                        leave="transition ease-in duration-75"
                                                        leaveFrom="transform opacity-100 scale-100"
                                                        leaveTo="transform opacity-0 scale-95"
                                                        className="text-red-500 text-sm ml-7"
                                                    >
                                                        {meta.error}
                                                    </Transition>
                                                </>
                                            )}
                                        />
                                        <button
                                            type="submit"
                                            className="text-center bg-[#9043B6] text-white font-semibold rounded-lg py-2 mt-6 w-full text-[18px] disabled:bg-gray-400 disabled:opacity-80"
                                            disabled={loading}
                                        >
                                            {loading ? (
                                                <i className="fa-solid fa-spinner  animate-spin duration-300"></i>
                                            ) : (
                                                'Submit'
                                            )}
                                        </button>
                                    </form>
                                )}
                            />
                        </div>
                    </div>
                    <div className="col-start-7 col-span-5">
                        <img src={applicationFormImage} alt="" />
                    </div>
                </div>
            </div>
        </>
    )
}
