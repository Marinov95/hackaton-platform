import { Link, useNavigate } from 'react-router-dom'
import useEndpoint from '../api/useEndpoint'
import Card from '../components/Card'
import clsx from 'clsx'
import { useState, useEffect } from 'react'
import fetchWithAuth from '../components/FetchWithAuth'
import dayjs from 'dayjs'
import ListItem from '../components/ListItem'

export default function HackatonDashboard() {
    const navigate = useNavigate()
    const [event, setEvent] = useState({} as any)
    const splitUrl = location.pathname.split('/')
    const hackatonId = splitUrl.pop()
    useEffect(() => {
        async function getHackaton() {
            try {
                const res = await fetch(
                    'https://hristina-m-kjiroski.sharedwithexpose.com/api/events'
                )
                const data = await res.json()
                console.log(data)
                setEvent(
                    data.event.find(
                        (event) =>
                            event.id.toString() === hackatonId?.toString()
                    )
                )
            } catch (err) {
                console.log(err)
            }
        }
        getHackaton()
        console.log(event)
    }, [])

    const participantsNum = getRandomInt(0, 200)
    const onlineParticipants = getRandomInt(0, participantsNum / 2)
    const liveParticipants = participantsNum - onlineParticipants
    console.log(event)
    return (
        <div className=" pr-14">
            {' '}
            <div className="flex [&>*]:flex-1 gap-10">
                <div className="bg-[#E3E0EF] p-4 rounded-xl shadow-md flex flex-col justify-between">
                    <div>
                        <h4 className="mb-4">Event</h4>
                        <h2 className="text-4xl mb-6 font-semibold">
                            Online matchmaking platform
                        </h2>
                        <Link
                            to={
                                event &&
                                'https://elaborate-fox-6aaaec.netlify.app/hackaton/' +
                                    hackatonId
                            }
                            target="_blank"
                            className={clsx(
                                event ? 'text-blue-500' : 'text-gray-500'
                            )}
                        >
                            {event &&
                                'https://elaborate-fox-6aaaec.netlify.app/hackaton/' +
                                    hackatonId}
                        </Link>
                    </div>
                    <div>
                        <p>
                            {event &&
                                formatDate(event.start_date, event.end_date)}
                        </p>
                    </div>
                </div>
                <div className="bg-[#46CAC9] p-4 rounded-xl shadow-md flex flex-col justify-between">
                    <h2 className="text-4xl mb-6 font-semibold">
                        Academies of the Hackathon
                    </h2>
                    <ul>
                        {event &&
                            event.academies &&
                            event.academies.map((academy) => (
                                <li key={academy.id}>{academy.name}</li>
                            ))}
                    </ul>
                </div>
                <div className="flex flex-col gap-4 justify-between">
                    <div className="bg-[#FCFCCC] p-3 rounded-xl shadow-md">
                        <h2 className="text-4xl mb-6 font-semibold">
                            Number of Participants
                        </h2>
                        <p className="text-3xl">{participantsNum}</p>
                    </div>
                    <div className="bg-[#FCFCCC] p-4 rounded-xl shadow-md flex justify-between items-center">
                        <h2 className="text-3xl font-semibold">Online</h2>
                        <p className="text-2xl">{onlineParticipants}</p>
                    </div>
                    <div className="bg-[#FCFCCC] p-4 rounded-xl shadow-md flex justify-between items-center">
                        <h2 className="text-3xl font-semibold">Live</h2>
                        <p className="text-2xl">{liveParticipants}</p>
                    </div>
                </div>
                <div className="bg-[#E3E0EF] p-4 rounded-xl shadow-md flex flex-col justify-between">
                    <div>
                        <h2 className="text-4xl mb-6 font-semibold">
                            Number of Groups
                        </h2>
                    </div>
                    <div>
                        <p className="text-2xl text-right">0</p>
                    </div>
                </div>
            </div>
            <div className="flex gap-10 pt-10">
                <div className="bg-[#F0F0F0] p-4 rounded-xl shadow-md flex [&>*]:flex-1 gap-6 basis-2/3">
                    <div className="bg-[#E3E0EF] p-4 rounded-xl shadow-md flex flex-col">
                        <h3 className="text-2xl font-semibold mb-6">
                            Academies of the Hackathon
                        </h3>
                        <ul className="flex flex-col gap-3">
                            {event &&
                                event.academies &&
                                event.academies.map((academy) => (
                                    <ListItem
                                        academy={academy}
                                        key={academy.id}
                                    />
                                ))}
                        </ul>
                    </div>
                    <div className="bg-[#E3E0EF] p-4 rounded-xl shadow-md flex flex-col">
                        <h3 className="text-2xl font-semibold mb-6">
                            Food Preferences
                        </h3>
                        <ul className="flex flex-col gap-3">
                            {event &&
                                event.academies &&
                                event.academies.map((academy) => (
                                    <ListItem
                                        academy={academy}
                                        key={academy.id}
                                    />
                                ))}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

function formatDate(startDate: Date, endDate: Date) {
    const parsedStartDate = dayjs(startDate).format('DD-MM-YYYY')
    const parsedEndDate = dayjs(endDate).format('DD-MM-YYYY')
    return `${parsedStartDate.slice(0, 2)} - ${parsedEndDate}`
}

function getRandomInt(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}
