import Card from '../components/Card'
import homeBanner from '../assets/images/home-banner.png'
import HackathonCardLinks from '../components/HackathonCardLinks'
import rightArrowIcon from '../assets/images/icons/right-arrow.png'
import checkIcon from '../assets/images/icons/check.png'
import alarmIcon from '../assets/images/icons/alarm.png'
import { Link } from 'react-router-dom'

import useEndpoint from '../api/useEndpoint'

export default function Home() {
    const startedEventsData = useEndpoint({
        endpoint:
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/events/started',
    })
    const upcomingEventsData = useEndpoint({
        endpoint:
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/events/upcoming',
    })
    const pastEventsData = useEndpoint({
        endpoint:
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/events/ended',
    })
    console.log(startedEventsData)
    console.log(upcomingEventsData)
    console.log(pastEventsData)
    return (
        <>
            <div className="grid grid-cols-6">
                <div className="col-span-2">
                    <h1 className="text-[55px] font-bold leading-tight">
                        Create a seamless experience for your event
                    </h1>
                    <p className="text-[#919191] text-[16px] mt-2">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Iste ea a qui magni enim officiis vel. Sapiente
                        ipsum totam reiciendis voluptatum soluta explicabo quasi
                        dignissimos suscipit, dicta laboriosam voluptate laborum
                        iusto voluptatem vel excepturi eum assumenda ab ut cum
                        id, nemo aut. Voluptatum officia quia sit dignissimos
                        mollitia, excepturi nihil.
                    </p>
                    <div className="mt-6">
                        <Link
                            to="/admin/create"
                            className="rounded-lg bg-[#333333] text-white px-14 py-2 text-[16px] font-bold"
                        >
                            Create Event
                        </Link>
                    </div>
                </div>
                <div className="col-span-4">
                    <img src={homeBanner} />
                </div>
                <div className="col-span-6 my-4">
                    <h4>Events preview </h4>
                </div>
                <div className="col-span-2">
                    <Card className="bg-gradient-to-b from-white to-[#3FC8C6] max-h-[400px] overflow-auto px-2 py-2 shadow-md shadow-gray-400">
                        <div className="flex items-center my-2 ml-3">
                            <img src={rightArrowIcon} className="mr-3" />
                            <span>In progress</span>
                        </div>
                        {startedEventsData.state.events &&
                        startedEventsData.state.events.length ? (
                            startedEventsData.state.events.map((event: any) => (
                                <HackathonCardLinks
                                    title={event.name}
                                    start_date={event.start_date}
                                    end_date={event.end_date}
                                    participants={event.participants}
                                    key={event.id}
                                />
                            ))
                        ) : (
                            <div className="p-3">No events</div>
                        )}
                    </Card>
                </div>
                <div className="col-span-2">
                    <Card className="bg-gradient-to-b from-white to-[#FEFE9D] max-h-[400px] overflow-auto px-2 py-2 shadow-md shadow-gray-400">
                        <div className="flex items-center my-2 ml-3">
                            <img src={alarmIcon} className="mr-3" />
                            <span>Upcoming</span>
                        </div>
                        {upcomingEventsData.state.events &&
                        upcomingEventsData.state.events.length ? (
                            upcomingEventsData.state.events.map(
                                (event: any) => (
                                    <HackathonCardLinks
                                        title={event.name}
                                        start_date={event.start_date}
                                        end_date={event.end_date}
                                        participants={event.participants}
                                        key={event.id}
                                    />
                                )
                            )
                        ) : (
                            <div className="p-3">No events</div>
                        )}
                    </Card>
                </div>
                <div className="col-span-2">
                    <Card className="bg-gradient-to-b from-white to-[#CACACA] max-h-[400px] overflow-auto px-2 py-2 shadow-md shadow-gray-400">
                        <div className="flex items-center my-2 ml-3">
                            <img src={checkIcon} className="mr-3" />
                            <span>Past</span>
                        </div>
                        {pastEventsData.state.events &&
                        pastEventsData.state.events.length ? (
                            pastEventsData.state.events.amp((event: any) => (
                                <HackathonCardLinks
                                    title={event.name}
                                    start_date={event.start_date}
                                    end_date={event.end_date}
                                    participants={event.participants}
                                    key={event.id}
                                />
                            ))
                        ) : (
                            <div className="p-3">No events</div>
                        )}
                    </Card>
                </div>
            </div>
        </>
    )
}
