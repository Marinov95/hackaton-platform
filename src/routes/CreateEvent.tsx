import Card from '../components/Card'
import { Form, Field } from 'react-final-form'
import InputGroup from '../components/InputGroup'
import { Dialog, Listbox, Transition } from '@headlessui/react'
import { Fragment, useRef, useState } from 'react'
import { ACADEMIES } from '../assets/constants/constants'
import logo from '../assets/images/brainster-logo-black.png'
import clsx from 'clsx'
import fetchWithAuth from '../components/FetchWithAuth'

export default function CreateEvent() {
    const [selectedAcademies, setSelected] = useState([ACADEMIES[0]])
    const [file, setFile] = useState(null as File | null)
    const [academiesError, setAcademiesError] = useState(false)
    let [isOpen, setIsOpen] = useState(false)
    const [generatedFormLink, setGeneratedFormLink] = useState(
        'https://elaborate-fox-6aaaec.netlify.app/hackaton/1'
    )
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState('')

    const [copied, setCopied] = useState(false)
    const uploadButtonElRef = useRef(null as any)

    const handleCopyClick = async () => {
        try {
            await navigator.clipboard.writeText(generatedFormLink)
            setCopied(true)
            setTimeout(() => {
                setCopied(false)
            }, 2000)
        } catch (error) {
            console.error('Failed to copy text: ', error)
        }
    }

    function closeModal() {
        setIsOpen(false)
        setCopied(false)
    }

    function openModal() {
        setIsOpen(true)
    }

    async function onSubmit(form: typeof Form) {
        console.log(form)
        if (selectedAcademies.length === 0) {
            return
        }
        console.log(selectedAcademies)

        const serializedData = {
            name: form.eventName,
            start_date: form.startDate,
            end_date: form.endDate,
            deadline: form.deadline,
            live: form.eventType1 ? 1 : 0,
            online: form.eventType2 ? 1 : 0,
            academy_id: selectedAcademies.map((academy) => academy.id),
            max_num_applicants: 75,
        }

        if (file) serializedData.file_name = file.name
        if (form.description) serializedData.description = form.description

        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        headers.append('Access-Control-Allow-Origin', '*')
        headers.append('Access-Control-Allow-Credentials', 'true')

        try {
            setLoading(true)
            const response = await fetch(
                'https://hristina-m-kjiroski.sharedwithexpose.com/api/events',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(serializedData),
                }
            )
            const data = await response.json()
            console.log(data)

            setLoading(false)
        } catch (err) {
            setLoading(false)
            setError('Something went wrong. Please try again later.')
            console.log(err)
        }
        openModal()
    }

    function validate(form: typeof Form) {
        const errors = [] as any
        console.log(form)

        if (!form.eventName) {
            errors.eventName = 'Required'
        }

        if (!form.startDate) {
            errors.startDate = 'Required'
        }

        if (!form.endDate) {
            errors.endDate = 'Required'
        }

        if (!form.deadline) {
            errors.deadline = 'Required'
        }

        if (
            (!form.eventType1 || !form.eventType1.length) &&
            (!form.eventType2 || !form.eventType2.length)
        ) {
            errors.eventType2 = 'Required'
        }

        if (selectedAcademies && !selectedAcademies.length) {
            errors.academies = 'Required'
            setAcademiesError(true)
        } else {
            setAcademiesError(false)
        }

        return errors
    }

    const handleUploadButtonClick = () => {
        if (uploadButtonElRef.current) {
            uploadButtonElRef.current.click() // Trigger the click event on the target element
        }
    }

    const handleFileUpload = (event: any) => {
        const file = event.target.files[0]
        // Handle the file upload logic
        setFile(file)
        console.log(file)
    }

    return (
        <>
            <div>
                <h1 className="text-[32px] font-bold">Create New Event</h1>
                <p className="w-[550px] text-[18px]">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Odio qui rem sequi numquam tempora porro nam maiores labore
                    sunt tempore.
                </p>
            </div>

            <Form
                onSubmit={onSubmit}
                validate={validate}
                render={({ handleSubmit }) => (
                    <form
                        onSubmit={handleSubmit}
                        className="grid grid-cols-12 mt-28"
                    >
                        <div className="col-start-2 col-span-5">
                            <Card className="py-16 px-6 bg-[#F3F0F0] space-y-3">
                                <div>
                                    <Field
                                        name="eventName"
                                        type="text"
                                        render={({ input, meta }) => (
                                            <InputGroup
                                                label="Name of the event"
                                                input={input}
                                                meta={meta}
                                                placeholder="Name of the event"
                                            />
                                        )}
                                    />
                                    <div className="py-3">
                                        <label className="font-semibold">
                                            Date of event
                                        </label>
                                        <div className="flex gap-2 mt-2">
                                            <div className="flex-1">
                                                <Field
                                                    name="startDate"
                                                    type="date"
                                                    render={({
                                                        input,
                                                        meta,
                                                    }) => (
                                                        <InputGroup
                                                            input={input}
                                                            meta={meta}
                                                        />
                                                    )}
                                                />
                                            </div>
                                            <div className="flex-1">
                                                <Field
                                                    name="endDate"
                                                    type="date"
                                                    render={({
                                                        input,
                                                        meta,
                                                    }) => (
                                                        <InputGroup
                                                            input={input}
                                                            meta={meta}
                                                        />
                                                    )}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <Field
                                        name="deadline"
                                        type="date"
                                        render={({ input, meta }) => (
                                            <InputGroup
                                                label="Deadline for applications"
                                                input={input}
                                                meta={meta}
                                                placeholder="Name of the event"
                                            />
                                        )}
                                    />
                                    <div className="my-4 flex">
                                        <label className="font-semibold mr-3">
                                            Type of event:
                                        </label>
                                        <div className="flex justify-around flex-1 relative">
                                            <Field
                                                name="eventType1"
                                                type="checkbox"
                                                value="live"
                                                render={({ input }) => (
                                                    <div className="flex items-center">
                                                        <input
                                                            type="checkbox"
                                                            name={input.name}
                                                            value="live"
                                                            id="live"
                                                            checked={
                                                                input.checked
                                                            }
                                                            onChange={
                                                                input.onChange
                                                            }
                                                            className={
                                                                'w-[25px] h-[25px] border-2'
                                                            }
                                                        />
                                                        <label
                                                            htmlFor="live"
                                                            className="ml-2 text-[16px]"
                                                        >
                                                            Live
                                                        </label>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="eventType2"
                                                type="checkbox"
                                                value="online"
                                                render={({ input, meta }) => (
                                                    <div className="flex items-center ">
                                                        <input
                                                            type="checkbox"
                                                            name={input.name}
                                                            value="online"
                                                            id="online"
                                                            checked={
                                                                input.checked
                                                            }
                                                            onChange={
                                                                input.onChange
                                                            }
                                                            className={
                                                                'w-[25px] h-[25px] border-2'
                                                            }
                                                        />
                                                        <label
                                                            htmlFor="online"
                                                            className="ml-2 text-[16px]"
                                                        >
                                                            Online
                                                        </label>
                                                        <Transition
                                                            as="span"
                                                            show={
                                                                meta.touched ||
                                                                meta.dirty
                                                            }
                                                            enter="transition ease-out duration-100"
                                                            enterFrom="transform opacity-0 scale-95"
                                                            enterTo="transform opacity-100 scale-100"
                                                            leave="transition ease-in duration-75"
                                                            leaveFrom="transform opacity-100 scale-100"
                                                            leaveTo="transform opacity-0 scale-95"
                                                            className="text-red-500 text-sm absolute right-0 bottom-0"
                                                        >
                                                            {meta.error}
                                                        </Transition>
                                                    </div>
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div>
                                        <label
                                            htmlFor="description"
                                            className="font-semibold my-3"
                                        >
                                            Info about the event/client
                                        </label>
                                        <Field
                                            name="description"
                                            type="textarea"
                                            component={'textarea'}
                                            placeholder="Write..."
                                            rows={4}
                                            id="description"
                                            className={
                                                'w-full border-2 border-black rounded-md resize-none p-2'
                                            }
                                        />
                                    </div>
                                </div>
                            </Card>
                        </div>
                        <div className="col-start-7 col-span-5 flex flex-col justify-between">
                            <Card className="py-16 px-6 bg-[#F3F0F0] space-y-3">
                                <label className="font-medium">
                                    Academies of the event
                                </label>
                                <Listbox
                                    value={selectedAcademies}
                                    onChange={setSelected}
                                    multiple
                                    className={clsx(
                                        'border-2 rounded-md',
                                        academiesError
                                            ? 'border-red-500'
                                            : 'border-black'
                                    )}
                                >
                                    <div className="relative mt-1">
                                        <Listbox.Button className="relative w-full cursor-default rounded-lg bg-white py-2 pl-3 pr-10 text-left shadow-md focus:outline-none sm:text-sm">
                                            <span className="block truncate h-[20px]">
                                                {selectedAcademies.length
                                                    ? selectedAcademies
                                                          .map(
                                                              (academy) =>
                                                                  academy.name
                                                          )
                                                          .join(', ')
                                                    : 'Select academies'}
                                            </span>
                                            <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                                <i className="fa-solid fa-chevron-down"></i>
                                            </span>
                                        </Listbox.Button>
                                        <Transition
                                            as={Fragment}
                                            leave="transition ease-in duration-100"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                        >
                                            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                {ACADEMIES.map(
                                                    (academy, academyIdx) => (
                                                        <Listbox.Option
                                                            key={academyIdx}
                                                            className={({
                                                                active,
                                                            }) =>
                                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                    active
                                                                        ? 'bg-gray-300'
                                                                        : 'text-gray-900'
                                                                }`
                                                            }
                                                            value={academy}
                                                        >
                                                            {({ selected }) => (
                                                                <>
                                                                    <span
                                                                        className={`block truncate ${
                                                                            selected
                                                                                ? 'font-medium'
                                                                                : 'font-normal'
                                                                        }`}
                                                                    >
                                                                        {
                                                                            academy.name
                                                                        }
                                                                    </span>
                                                                    {selected ? (
                                                                        <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                                                            <i className="fa-regular fa-circle-check"></i>
                                                                        </span>
                                                                    ) : (
                                                                        <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                                                            <i className="fa-regular fa-circle"></i>
                                                                        </span>
                                                                    )}
                                                                </>
                                                            )}
                                                        </Listbox.Option>
                                                    )
                                                )}
                                            </Listbox.Options>
                                        </Transition>
                                    </div>
                                </Listbox>
                                <p
                                    className={clsx(
                                        'text-red-500 text-sm',
                                        !academiesError && 'hidden'
                                    )}
                                >
                                    Required
                                </p>
                                <div>
                                    <label className="font-medium">
                                        Upload
                                    </label>
                                    <div
                                        className="border-2 border-black rounded-md h-[38px] px-2 flex items-center justify-between mt-2"
                                        onClick={handleUploadButtonClick}
                                    >
                                        <input
                                            ref={uploadButtonElRef}
                                            type="file"
                                            className="hidden"
                                            onChange={handleFileUpload}
                                        />
                                        <span className="text">
                                            {file
                                                ? file.name
                                                : 'Upload a file...'}
                                        </span>
                                        <div className="ml-auto cursor-pointer">
                                            <i className="fa-solid fa-file-arrow-up"></i>
                                        </div>
                                    </div>
                                </div>
                            </Card>
                            <div className="self-end mr-10">
                                <button
                                    type="submit"
                                    className="rounded-lg bg-[#333333] text-white px-14 py-2 text-[16px] font-bold"
                                >
                                    {loading ? (
                                        <i className="fa-solid fa-spinner  animate-spin duration-300"></i>
                                    ) : (
                                        'Create Event'
                                    )}
                                </button>
                            </div>
                        </div>
                    </form>
                )}
            />
            <Transition appear show={isOpen} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={closeModal}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-black bg-opacity-25" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-y-auto">
                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="transform overflow-hidden rounded-xl h-[468px] w-[648px] bg-white p-6 text-left align-middle shadow-xl transition-all">
                                    <div
                                        onClick={closeModal}
                                        className="flex content-center cursor-pointer w-fit"
                                    >
                                        <i className="fa-solid fa-chevron-left text-[23px] mr-2"></i>
                                        <span className="text-[16px] font-medium">
                                            Back
                                        </span>
                                    </div>
                                    <Dialog.Title
                                        as="h3"
                                        className="text-xl font-medium text-center"
                                    >
                                        Success!
                                    </Dialog.Title>
                                    <div className="mt-[73px] text-center">
                                        <h1 className="text-[32px] font-bold">
                                            The Event is Created!
                                        </h1>
                                    </div>
                                    <div className="mt-4 text-center">
                                        <input
                                            type="text"
                                            className="w-[410px] py-2 text-center text-[#6792FF] border-2 border-[#919191] rounded-md"
                                            value={generatedFormLink}
                                            readOnly
                                        />
                                    </div>
                                    <div className="mt-7 text-center">
                                        <button
                                            type="button"
                                            className={clsx(
                                                'inline-flex justify-center rounded-md border border-transparent px-4 py-2 text-[16px] font-medium text-white min-w-[194px]',
                                                copied
                                                    ? 'bg-[#4ce85e]'
                                                    : 'bg-[#9043B6]'
                                            )}
                                            onClick={handleCopyClick}
                                        >
                                            {copied
                                                ? 'Successfully copied!'
                                                : 'Copy link'}
                                        </button>
                                    </div>
                                    <div className="flex justify-center mt-[75px]">
                                        <img src={logo} />
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    )
}
