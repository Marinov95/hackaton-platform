import { Transition } from '@headlessui/react'
import clsx from 'clsx'

type Props = {
    label?: string
    input: any
    meta?: any
    alternativestyle?: boolean
} & JSX.IntrinsicElements['input']

export default function InputGroup(props: Props) {
    const { label, input, meta, ...inputProps } = props
    const hasError = meta.touched && meta.error
    return (
        <>
            {!props.alternativestyle ? (
                <>
                    <div className={clsx('flex gap-1 flex-col')}>
                        {label && (
                            <label className="font-semibold">
                                {props.label}
                            </label>
                        )}
                        <input
                            className={clsx(
                                'px-3 py-2 border-2 rounded-md outline-none w-full transition-all duration-300 focus:bg-gray-100',
                                hasError ? 'border-red-500' : 'border-black'
                            )}
                            {...input}
                            {...inputProps}
                        />

                        <Transition
                            as="p"
                            show={hasError ? true : false}
                            enter="transition ease-out duration-100"
                            enterFrom="transform opacity-0 scale-95"
                            enterTo="transform opacity-100 scale-100"
                            leave="transition ease-in duration-75"
                            leaveFrom="transform opacity-100 scale-100"
                            leaveTo="transform opacity-0 scale-95"
                            className="text-red-500 text-sm"
                        >
                            {meta.error}
                        </Transition>
                    </div>
                </>
            ) : (
                <>
                    <div className="h-[136px] flex flex-col justify-between py-3 px-6 rounded-md border-2 border-[#D9D9D9]">
                        <label htmlFor={input.name}>{props.label}</label>

                        <input
                            type="text"
                            className={clsx(
                                hasError
                                    ? 'border-red-500'
                                    : 'border-[#D9D9D9]',
                                'w-full h-[40px] border-b-2 focus:outline-none'
                            )}
                            {...inputProps}
                            {...input}
                        />
                    </div>
                    <Transition
                        as="p"
                        show={hasError ? true : false}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                        className="text-red-500 text-sm ml-7"
                    >
                        {meta.error}
                    </Transition>
                </>
            )}
        </>
    )
}
