export default function fetchWithAuth(url: string, options = {} as any) {
    // Get the token from your preferred source (e.g., local storage, state management)
    const token = localStorage.getItem('token') || ''

    // Add the bearer token to the headers
    const headers = {
        ...options.headers,
        Authorization: `Bearer ${token}`,
    }

    // Make the fetch request with the updated headers
    return fetch(`${url}`, { ...options, headers })
}
