import dayjs from 'dayjs'

type Props = {
    title: string
    start_date: string
    end_date: string
}

export default function HackathonCardLinks(props: Props) {
    const parsedStartDate = dayjs(props.start_date).format('DD-MM-YYYY')
    const parsedEndDate = dayjs(props.start_date).format('DD-MM-YYYY')
    const parsedDate = `${parsedStartDate.slice(0, 2)} - ${parsedEndDate}`
    return (
        <>
            <div className="rounded-xl px-5 py-2 my-3 bg-white bg-opacity-60">
                <h1>{props.title}</h1>
                <p>{parsedDate}</p>
            </div>
        </>
    )
}
