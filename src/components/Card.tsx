import clsx from 'clsx'

type Props = {
    children: React.ReactNode
    className: string
}

export default function Card(props: Props) {
    return (
        <>
            <div
                className={clsx(
                    'rounded-2xl w-[95%]',
                    props.className && props.className
                )}
            >
                {props.children}
            </div>
        </>
    )
}
