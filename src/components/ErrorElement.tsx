import { Link } from 'react-router-dom'

export default function ErrorElement() {
    return (
        <div className="h-screen w-full grid place-items-center">
            <div className="rounded-md border border-red-500 p-5 space-y-3 text-center bg-gray-300">
                <h1 className="text-3xl font-bold">Ooops</h1>
                <h2>Something Went Wrong</h2>
                <p>The page you are looking for does not exist.</p>
                <Link to="/admin/home" className="block">
                    <button className="bg-black text-white px-3 py-2 border border-white transition-all duration-200 hover:text-black hover:bg-white hover:border-black">
                        Go Back
                    </button>
                </Link>
            </div>
        </div>
    )
}
