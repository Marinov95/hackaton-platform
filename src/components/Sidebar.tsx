import { NavLink, useNavigate } from 'react-router-dom'
import logo from '../assets/images/brainster-logo.png'
import clsx from 'clsx'
import useEndpoint from '../api/useEndpoint'
import { Menu, Transition } from '@headlessui/react'

const navigation = [
    {
        name: 'Home',
        path: '/admin/home',
    },
    {
        name: 'Create',
        path: '/admin/create',
    },
]

export default function Sidebar() {
    const navigate = useNavigate()
    const { state: events } = useEndpoint({
        endpoint: 'https://hristina-m-kjiroski.sharedwithexpose.com/api/events',
    })

    const navLinks = navigation.map((item) => (
        <NavLink
            to={item.path}
            key={item.path}
            className={({ isActive }) =>
                clsx(
                    'text-white pl-6 border-l-4',
                    isActive
                        ? 'border-white transition-all duration-300'
                        : 'border-transparent'
                )
            }
        >
            <span>{item.name}</span>
        </NavLink>
    ))

    function logoutHandler() {
        localStorage.removeItem('token')
        navigate('/')
    }
    return (
        <>
            <div className="w-[214px] bg-[#9043B6] min-h-screen mr-14 relative">
                <div className="flex flex-col h-screen sticky top-0">
                    <div className="bg-[#AD7AC7] h-[40px] flex items-center justify-center">
                        <p className="text-white text-[16px]">
                            Admin Dashboard
                        </p>
                    </div>
                    <div className="flex flex-1 flex-col justify-between">
                        <div>
                            <div className="mt-6">
                                <img src={logo} alt="logo" className="pl-6" />
                            </div>
                            <div className="flex flex-col gap-2 mt-6">
                                {navLinks}
                                <Menu
                                    as="div"
                                    className="relative inline-block text-left"
                                >
                                    <div>
                                        <Menu.Button className="text-white pl-6 border-l-4 flex items-center justify-between border-transparent w-full pr-6">
                                            <span className="block">
                                                Dashboard
                                            </span>
                                            <i className="fa-solid fa-chevron-right ml-auto block"></i>
                                        </Menu.Button>
                                    </div>
                                    <Transition
                                        as="div"
                                        enter="transition ease-out duration-100"
                                        enterFrom="transform opacity-0 scale-95"
                                        enterTo="transform opacity-100 scale-100"
                                        leave="transition ease-in duration-75"
                                        leaveFrom="transform opacity-100 scale-100"
                                        leaveTo="transform opacity-0 scale-95"
                                    >
                                        <Menu.Items className="absolute border-l border-white right-0 -mt-7 left-0 top-0 w-full translate-x-full origin-top-rightrounded-md bg-violet-500 text-white shadow-lg">
                                            {events.event?.length > 0
                                                ? events.event.map((event) => {
                                                      return (
                                                          <NavLink
                                                              to={
                                                                  '/admin/dashboard/' +
                                                                  event.id
                                                              }
                                                              key={event.id}
                                                              className={({
                                                                  isActive,
                                                              }) =>
                                                                  clsx(
                                                                      'px-1 py-1 block transition-all duration-300',
                                                                      isActive &&
                                                                          'font-bold'
                                                                  )
                                                              }
                                                          >
                                                              <Menu.Item>
                                                                  {() => (
                                                                      <button className="">
                                                                          {
                                                                              event.name
                                                                          }
                                                                      </button>
                                                                  )}
                                                              </Menu.Item>
                                                          </NavLink>
                                                      )
                                                  })
                                                : ''}
                                        </Menu.Items>
                                    </Transition>
                                </Menu>
                            </div>
                        </div>
                        <div className="mb-16 pl-6 text-white text-xl space-y-2">
                            <p>Settings</p>
                            <button
                                type="button"
                                className="transition-all duration-300 hover:scale-105"
                                onClick={logoutHandler}
                            >
                                Log out
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
