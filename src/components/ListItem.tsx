import { Disclosure } from '@headlessui/react'
import clsx from 'clsx'

type Props = {
    academy: any
    key: string
}
export default function ListItem({ academy }: Props) {
    return (
        <Disclosure as="li">
            {({ open }) => (
                <>
                    <Disclosure.Button className="flex w-full justify-between items-center rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>{academy.name}</span>
                        <i
                            className={clsx(
                                'fa-solid fa-chevron-right h-5 w-5 text-purple-500 transition-all duration-200',
                                open && '-rotate-90 transform'
                            )}
                        />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500 bg-[#F0F0F0]">
                        No students from this academy is currently attenting the
                        hackathon.
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    )
}
