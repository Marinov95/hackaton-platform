import React from 'react'
import ReactDOM from 'react-dom/client'
import {
    createBrowserRouter,
    RouterProvider,
} from '../node_modules/react-router-dom/dist/index'
import App from './App.tsx'
import './index.css'
import Home from './routes/Home.tsx'
import Admin from './routes/Admin.tsx'
import CreateEvent from './routes/CreateEvent.tsx'
import Hackaton from './routes/Hackaton.tsx'
import Login from './routes/Login.tsx'
import HackatonDashboard from './routes/HackatonDashboard.tsx'
import ErrorElement from './components/ErrorElement.tsx'

const router = createBrowserRouter([
    {
        path: '/',
        element: <Admin />,
    },
    {
        path: '/admin',
        element: <Admin />,
        errorElement: <ErrorElement />,
        children: [
            {
                path: '/admin/home',
                element: <Home />,
            },
            {
                path: '/admin/create',
                element: <CreateEvent />,
            },
            {
                path: '/admin/dashboard/:hackatonId',
                element: <HackatonDashboard />,
            },
        ],
    },
    { path: '/hackaton/:hackatonId', element: <Hackaton /> },
    { path: '/login', element: <Login /> },
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <RouterProvider router={router} />
)
