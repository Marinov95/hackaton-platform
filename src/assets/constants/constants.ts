export const ACADEMIES = [
    {
        id: 1,
        name: 'Full Stack',
    },
    {
        id: 2,
        name: 'UX/UI',
    },
    {
        id: 3,
        name: 'Front End',
    },
    {
        id: 4,
        name: 'PM',
    },
    {
        id: 5,
        name: 'QA',
    },
    {
        id: 6,
        name: 'Graphic Design',
    },
    {
        id: 7,
        name: 'Marketing',
    },
]
