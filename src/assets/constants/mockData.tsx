export const startedEvents = {
    events: [
        {
            id: 1,
            name: 'Lorem ipsum dolor sit amet.',
            start_date: '11.05.2023',
            end_date: '20.05.2023',
            deadline: '09.05.2023',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl eget aliquam ultricies, nunc nisl ultricies nunc, eget ultricies nisl nisl eget nisl. Nulla euismod, nisl eget aliquam ultricies, nunc nisl ultricies nunc, eget ultricies nisl nisl eget nisl.',
            file_name: null,
            live: 1,
            online: 1,
            participants: 130,
        },
        {
            id: 2,
            name: 'Lorem ipsum dolor sit amet ono loat roeasd.',
            start_date: '12.05.2023',
            end_date: '23.05.2023',
            deadline: '10.05.2023',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl eget aliquam ultricies, nunc nisl ultricies nunc, eget ultricies nisl nisl eget nisl. Nulla euismod, nisl eget aliquam ultricies, nunc nisl ultricies nunc, eget ultricies nisl nisl eget nisl.',
            file_name: null,
            live: 1,
            online: 0,
            participants: 130,
        },
    ],
}

export const upcomingEvents = {
    events: [
        {
            id: 3,
            name: 'Lorem ipsum dolor sit amet.',
            start_date: '25.05.2023',
            end_date: '30.05.2023',
            deadline: '09.05.2023',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            file_name: null,
            live: 1,
            online: 1,
            participants: 130,
        },
    ],
}

export const pastEvents = {
    events: [
        {
            id: 4,
            name: 'Lorem ipsum dolor sit amet.',
            start_date: '25.05.2023',
            end_date: '30.05.2023',
            deadline: '09.05.2023',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            file_name: null,
            live: 1,
            online: 1,
            participants: 130,
        },
    ],
}
