import { useState, useEffect } from 'react'
import fetchWithAuth from '../components/FetchWithAuth'

type Props = {
    endpoint: string
}

export default function useEndpoint({ endpoint }: Props) {
    const [state, setState] = useState([] as any)
    async function fetchData() {
        try {
            const result = await fetchWithAuth(endpoint)
            const data = await result.json()
            setState(data)
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
    return { state: state }
}
