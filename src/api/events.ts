const headers = new Headers()
headers.append('Content-Type', 'application/json')
headers.append('Accept', 'application/json')

headers.append('Access-Control-Allow-Origin', '*')
headers.append('Access-Control-Allow-Credentials', 'true')

export async function getEvents() {
    return (
        await fetch(
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/events/'
        )
    ).json()
}
export async function getEventsInProgress() {
    return (
        await fetch(
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/events/started'
        )
    ).json()
}
export async function getUpcomingEvents() {
    return (
        await fetch(
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/upcoming'
        )
    ).json()
}
export async function getFinishedEvents() {
    return (
        await fetch(
            'https://hristina-m-kjiroski.sharedwithexpose.com/api/ended'
        )
    ).json()
}
